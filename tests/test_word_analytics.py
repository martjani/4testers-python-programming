from src.word_analytics import filter_words_containing_letter_a

def test_filtering_words_for_empty_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []

def test_filtering_words_for_list_of_integers():
    filtered_list = filter_words_containing_letter_a([1, 2, 3])
    assert filtered_list == []

def test_filtering_words_for_list_of_special_characters():
    filtered_list = filter_words_containing_letter_a(['@', '!'])
    assert filtered_list == []

def test_filtering_all_words_containing_with_letter_a():
    filtered_list = filter_words_containing_letter_a(['paka', 'maka', 'buka'])
    assert filtered_list == []

def test_filtering_words_not_containing_letter_a():
    filtered_list = filter_words_containing_letter_a(['pies', 'kot', 'smok'])
    assert filtered_list == []

def test_filtering_words_for_mixed_list_of_single_values():
    filtered_list = filter_words_containing_letter_a([1, 'a', 2, 'b'])
    assert filtered_list == []

[]
[1, 2, 3]
['@', '!']
['pies','kot', 'smok']
['a', 'b']
[1, 'a', 2, 'b']
['paka', 'maka', 'buka']
['kot', 'żaba']
