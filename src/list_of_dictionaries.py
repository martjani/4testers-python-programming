animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2= {
    "kind": "cat",
    "age": 5,
    "male": False
}

animal_kinds = ['dog', 'cat', 'fish']

animals = [
 {
    "kind": "dog",
    "age": 2,
    "male": True
},
 {
    "kind": "cat",
    "age": 5,
    "male": False
},
{
    "kind": "fish",
    "age": 1,
    "male": False
}
    ]
print(len(animals))
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("The age of the last animal is equal", last_animal_age)
print(animals[0]["male"])
print(animals[1]["male"])

animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": False
    }
)
print(animals)
