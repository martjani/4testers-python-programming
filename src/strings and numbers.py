first_name = "Marta"
last_name = "Janicka"
email = "gggg@ggg.com"

print("Mam na imię", first_name, ". ", "Moje nazwisko to", last_name, ".", sep=" ")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-string

my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}. \nMój email to {email}."
print(my_bio_using_f_string)

print(f"wynik operacji mnożenia 4 przez 5 to {4*5}")

# Algebra

area_of_the_circle_with_radius_5 = 3.1415 * 5 ** 2
print(area_of_the_circle_with_radius_5)


