# def display_speed_information(speed):
#     # if speed <= 50:
#     #     print("Thank you, your speed is below limit!")
#     # else:
#     #     print("Slow down!")
#
#     if speed > 50:
#         print("Slow down!")
#     else:
#         print("Thank you, your speed is below limit!")
#
# if __name__ == '__main__':
#     display_speed_information(50)
#     display_speed_information(49)
#     display_speed_information(51)

# def check_normal_conditions(temperature_in_celsius, pressure_in_hectopascal):
#     if temperature_in_celsius == 0 and pressure_in_hectopascal == 1013:
#         return True
#     else:
#         return False
#
# if __name__ == '__main__':
#     print(check_normal_conditions(0, 1013))
#     print(check_normal_conditions(1, 1013))
#     print(check_normal_conditions(0, 1014))
#     print(check_normal_conditions(1, 1014))

# def calculate_fine_amount(speed):
#     return 500 + (speed - 50) * 10

# def print_of_value_of_speeding_fine_in_built_up_area(speed):
#     print("your speed was:", speed)
#     if speed > 100:
#         print('You just lost your driving licences')
#     elif speed > 50:
#         print(f'You just got a fine. Fine amount {calculate_fine_amount(speed)}')
#     else:
#         print('You are good to go')
#
# if __name__ == '__main__':
#     print_of_value_of_speeding_fine_in_built_up_area(101)
#     print_of_value_of_speeding_fine_in_built_up_area(100)
#     print_of_value_of_speeding_fine_in_built_up_area(50)
#     print_of_value_of_speeding_fine_in_built_up_area(51)
#     print_of_value_of_speeding_fine_in_built_up_area(49)

def get_grade_info(grade):
    if not isinstance(grade, float) or isinstance(grade, int):
        return "N/A"
    elif grade < 2 or grade > 5:
        return "N/A"
    elif grade >= 4.5:
        return "Bardzo dobrze"
    elif grade >= 4.0:
        return "Dobry"
    elif grade >= 3.0:
        return "Dostateczny"
    else:
        return "Niedostateczny"

if __name__ == '__main__':
    print(get_grade_info(4.5))
    print(get_grade_info(6))
    print(get_grade_info(0))
    print(get_grade_info(3))

