import random
import datetime

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def get_random_female_name():
    return random.choice(female_fnames)

def get_random_country():
    return random.choice(countries)

def get_random_male_name():
    return random.choice(male_fnames)

def get_random_surname():
    return random.choice(surnames)

def get_random_email(name, surname):
    domain = "example.com"
    return f'{name.lower()}.{surname.lower()}@{domain}'

def get_random_age():
    return random.randint(5,45)

def check_age(age):
    if age >= 18:
        return True
    else:
        return False

def get_birth_year(age):
    current_day = datetime.date.today()
    return current_day.year - age

def get_dictionary_with_random_personal_data(female):
    if female == False:
        random_name = get_random_male_name()
    else:
        random_name = get_random_female_name()
    random_surname = get_random_surname()
    random_country = get_random_country()
    random_email = get_random_email(random_name, random_surname)
    random_age = get_random_age()
    is_adult = check_age(random_age)
    birth_year = get_birth_year(random_age)

    return {
       "name": random_name,
        "surname": random_surname,
        "country": random_country,
        "email": random_email,
        "age": random_age,
        "is adult": is_adult,
        "birth_year": birth_year,

    }
def grettings(name, surname, country, year):
    print(f"Hi! I'm {name} {surname}. I come from {country} and I was born in {year}")

def generate_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(0, number_of_dictionaries):
        if number % 2 == 0:
            female = True
        else:
            female = False
        list_of_dictionaries.append(get_dictionary_with_random_personal_data(female))
    return list_of_dictionaries




if __name__ == '__main__':
    list = generate_list_of_dictionaries_with_random_personal_data(10)
    for i in list:
        grettings(i['name'], i['surname'], i['country'], i['birth_year'])

