# lists
shopping_list = ['oranges', 'tofu', 'broccoli', 'pasta', 'tissues']
print(shopping_list[0])
print(shopping_list[1])
print(shopping_list[-1])
print(shopping_list[-2])

shopping_list.append('lemons')
print(shopping_list[5])

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_three_shopping_itmes = shopping_list[0:3]
print(first_three_shopping_itmes)

# dictionaries

animal = {
    "name": "`burek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal['age']
print("Dog age:", dog_age)

dog_name = animal['name']
print("Dog name:", dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Staszek"
print(animal)
