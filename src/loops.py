from random import randint
from math import sqrt


# def print_hello_40_times():
#     for i in range(1, 41):
#         print("Hello", i)
#
# def print_positive_numbers_divisible_by_7(start, stop):
#     for i in range(start, stop +1):
#         if i % 7 == 0:
#             print(i)
#
# def print_n_random_number(n):
#     for i in range(n):
#         print(f' loop {i}:', randint(1, 1000))

# def print_square_roots_of_numbers(list_of_numbers):
#     for number in list_of_numbers:
#         print(sqrt(number))

# def get_square_roots_of_measurements(list_of_measurements):
#     square_roots = []
#     for measurement in list_of_measurements:
#         square_roots.append(square_roots(measurement))
#     return square_roots

def map_list_of_temperatures_from_celsius_to_farenheit(temps_celsius):
    temps_farenheit = []
    for temp in temps_celsius:
        temp_f = temp * 9 / 5 + 32
        temps_farenheit.append(temp_f)
    return temps_farenheit

if __name__ == '__main__':
    # print_hello_40_times()
    # print_positive_numbers_divisible_by_7(1, 100)
    # print_n_random_number(20)
    # print(sqrt(9))
    # list_of_measurement_results = [11.0, 123, 78, 982, 4, 99]
    # print_square_roots_of_numbers(list_of_measurement_results)
    # square_roots_of_measurements = get_square_roots_of_measurements(list_of_measurements_results)
    # print(square_roots_of_measurements)

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(map_list_of_temperatures_from_celsius_to_farenheit(temps_celsius))
