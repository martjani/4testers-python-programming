animals = [
    {"name": 'burek', "kind": 'dog', "age": 7},
    {"name": 'bonifacy', "kind": 'cate', "age": None},
    {"name": 'misio', "kind": 'hamster', "age": 1},
]

print(animals[-1]["name"])
animals[1]["age"] = 2
print(animals[1])
animals.insert(0, {"name": 'Reksio', "kind": "dog", "age": 4})
print(animals)


addresses =[
    {"city": 'Wroclaw', "street": 'Legnicka', "house_number": '10', "post_code": "56123"},
    {"city": 'Krakow', "street": 'Szybowcowa', "house_number": '67', "post_code": "98768"},
    {"city": 'Warszawa', "street": 'Powstancow', "house_number": '54', "post_code": "93780"}
]
print(addresses[-1]["post_code"])
print(addresses[1]["city"])
addresses[0]["street"] = 'jesionowa'
print(addresses)
